# Perso Archimate Repo

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/thomas.plantain/archimate.git
git branch -M main
git push -uf origin main
```

## Description
Archimate models 
- https://www.archimatetool.com/
- [Open Group TOGAF](https://www.opengroup.org/togaf)


## Support
- thomas.plantain@gmail.com


## Project status
POC
